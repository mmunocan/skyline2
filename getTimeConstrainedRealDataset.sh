#! /usr/bin/env bash

# Dont forget
# javac libs/*.java
# make

# 0.01%
./getTimeConstrained dataset-real/snap00.1m 495000 505000 495000 505000 1
./getTimeConstrained dataset-real/snap01.1m 495000 505000 495000 505000 1
./getTimeConstrained dataset-real/snap02.1m 495000 505000 495000 505000 1
./getTimeConstrained dataset-real/snap03.1m 495000 505000 495000 505000 1
./getTimeConstrained dataset-real/snap04.1m 495000 505000 495000 505000 1
./getTimeConstrained dataset-real/snap05.1m 495000 505000 495000 505000 1
./getTimeConstrained dataset-real/snap06.1m 495000 505000 495000 505000 1
./getTimeConstrained dataset-real/snap07.1m 495000 505000 495000 505000 1
./getTimeConstrained dataset-real/snap08.1m 495000 505000 495000 505000 1
./getTimeConstrained dataset-real/snap09.1m 495000 505000 495000 505000 1
./getTimeConstrained dataset-real/snap10.1m 495000 505000 495000 505000 1
./getTimeConstrained dataset-real/snap11.1m 495000 505000 495000 505000 1

# 0.1%
./getTimeConstrained dataset-real/snap00.1m 484189 515812 484189 515812 1
./getTimeConstrained dataset-real/snap01.1m 484189 515812 484189 515812 1
./getTimeConstrained dataset-real/snap02.1m 484189 515812 484189 515812 1
./getTimeConstrained dataset-real/snap03.1m 484189 515812 484189 515812 1
./getTimeConstrained dataset-real/snap04.1m 484189 515812 484189 515812 1
./getTimeConstrained dataset-real/snap05.1m 484189 515812 484189 515812 1
./getTimeConstrained dataset-real/snap06.1m 484189 515812 484189 515812 1
./getTimeConstrained dataset-real/snap07.1m 484189 515812 484189 515812 1
./getTimeConstrained dataset-real/snap08.1m 484189 515812 484189 515812 1
./getTimeConstrained dataset-real/snap09.1m 484189 515812 484189 515812 1
./getTimeConstrained dataset-real/snap10.1m 484189 515812 484189 515812 1
./getTimeConstrained dataset-real/snap11.1m 484189 515812 484189 515812 1

# 1%
./getTimeConstrained dataset-real/snap00.1m 450000 550000 450000 550000 1
./getTimeConstrained dataset-real/snap01.1m 450000 550000 450000 550000 1
./getTimeConstrained dataset-real/snap02.1m 450000 550000 450000 550000 1
./getTimeConstrained dataset-real/snap03.1m 450000 550000 450000 550000 1
./getTimeConstrained dataset-real/snap04.1m 450000 550000 450000 550000 1
./getTimeConstrained dataset-real/snap05.1m 450000 550000 450000 550000 1
./getTimeConstrained dataset-real/snap06.1m 450000 550000 450000 550000 1
./getTimeConstrained dataset-real/snap07.1m 450000 550000 450000 550000 1
./getTimeConstrained dataset-real/snap08.1m 450000 550000 450000 550000 1
./getTimeConstrained dataset-real/snap09.1m 450000 550000 450000 550000 1
./getTimeConstrained dataset-real/snap10.1m 450000 550000 450000 550000 1
./getTimeConstrained dataset-real/snap11.1m 450000 550000 450000 550000 1

# 10%
./getTimeConstrained dataset-real/snap00.1m 341886 658114 341886 658114 1
./getTimeConstrained dataset-real/snap01.1m 341886 658114 341886 658114 1
./getTimeConstrained dataset-real/snap02.1m 341886 658114 341886 658114 1
./getTimeConstrained dataset-real/snap03.1m 341886 658114 341886 658114 1
./getTimeConstrained dataset-real/snap04.1m 341886 658114 341886 658114 1
./getTimeConstrained dataset-real/snap05.1m 341886 658114 341886 658114 1
./getTimeConstrained dataset-real/snap06.1m 341886 658114 341886 658114 1
./getTimeConstrained dataset-real/snap07.1m 341886 658114 341886 658114 1
./getTimeConstrained dataset-real/snap08.1m 341886 658114 341886 658114 1
./getTimeConstrained dataset-real/snap09.1m 341886 658114 341886 658114 1
./getTimeConstrained dataset-real/snap10.1m 341886 658114 341886 658114 1
./getTimeConstrained dataset-real/snap11.1m 341886 658114 341886 658114 1

# 25%
./getTimeConstrained dataset-real/snap00.1m 250000 750000 250000 750000 1
./getTimeConstrained dataset-real/snap01.1m 250000 750000 250000 750000 1
./getTimeConstrained dataset-real/snap02.1m 250000 750000 250000 750000 1
./getTimeConstrained dataset-real/snap03.1m 250000 750000 250000 750000 1
./getTimeConstrained dataset-real/snap04.1m 250000 750000 250000 750000 1
./getTimeConstrained dataset-real/snap05.1m 250000 750000 250000 750000 1
./getTimeConstrained dataset-real/snap06.1m 250000 750000 250000 750000 1
./getTimeConstrained dataset-real/snap07.1m 250000 750000 250000 750000 1
./getTimeConstrained dataset-real/snap08.1m 250000 750000 250000 750000 1
./getTimeConstrained dataset-real/snap09.1m 250000 750000 250000 750000 1
./getTimeConstrained dataset-real/snap10.1m 250000 750000 250000 750000 1
./getTimeConstrained dataset-real/snap11.1m 250000 750000 250000 750000 1

# 50%
./getTimeConstrained dataset-real/snap00.1m 146447 853554 146447 853554 1
./getTimeConstrained dataset-real/snap01.1m 146447 853554 146447 853554 1
./getTimeConstrained dataset-real/snap02.1m 146447 853554 146447 853554 1
./getTimeConstrained dataset-real/snap03.1m 146447 853554 146447 853554 1
./getTimeConstrained dataset-real/snap04.1m 146447 853554 146447 853554 1
./getTimeConstrained dataset-real/snap05.1m 146447 853554 146447 853554 1
./getTimeConstrained dataset-real/snap06.1m 146447 853554 146447 853554 1
./getTimeConstrained dataset-real/snap07.1m 146447 853554 146447 853554 1
./getTimeConstrained dataset-real/snap08.1m 146447 853554 146447 853554 1
./getTimeConstrained dataset-real/snap09.1m 146447 853554 146447 853554 1
./getTimeConstrained dataset-real/snap10.1m 146447 853554 146447 853554 1
./getTimeConstrained dataset-real/snap11.1m 146447 853554 146447 853554 1

# 99%
./getTimeConstrained dataset-real/snap00.1m 2507 997494 2507 997494 1
./getTimeConstrained dataset-real/snap01.1m 2507 997494 2507 997494 1
./getTimeConstrained dataset-real/snap02.1m 2507 997494 2507 997494 1
./getTimeConstrained dataset-real/snap03.1m 2507 997494 2507 997494 1
./getTimeConstrained dataset-real/snap04.1m 2507 997494 2507 997494 1
./getTimeConstrained dataset-real/snap05.1m 2507 997494 2507 997494 1
./getTimeConstrained dataset-real/snap06.1m 2507 997494 2507 997494 1
./getTimeConstrained dataset-real/snap07.1m 2507 997494 2507 997494 1
./getTimeConstrained dataset-real/snap08.1m 2507 997494 2507 997494 1
./getTimeConstrained dataset-real/snap09.1m 2507 997494 2507 997494 1
./getTimeConstrained dataset-real/snap10.1m 2507 997494 2507 997494 1
./getTimeConstrained dataset-real/snap11.1m 2507 997494 2507 997494 1

# 100%
./getTimeConstrained dataset-real/snap00.1m 0 1000000 0 1000000 1
./getTimeConstrained dataset-real/snap01.1m 0 1000000 0 1000000 1
./getTimeConstrained dataset-real/snap02.1m 0 1000000 0 1000000 1
./getTimeConstrained dataset-real/snap03.1m 0 1000000 0 1000000 1
./getTimeConstrained dataset-real/snap04.1m 0 1000000 0 1000000 1
./getTimeConstrained dataset-real/snap05.1m 0 1000000 0 1000000 1
./getTimeConstrained dataset-real/snap06.1m 0 1000000 0 1000000 1
./getTimeConstrained dataset-real/snap07.1m 0 1000000 0 1000000 1
./getTimeConstrained dataset-real/snap08.1m 0 1000000 0 1000000 1
./getTimeConstrained dataset-real/snap09.1m 0 1000000 0 1000000 1
./getTimeConstrained dataset-real/snap10.1m 0 1000000 0 1000000 1
./getTimeConstrained dataset-real/snap11.1m 0 1000000 0 1000000 1